/**
 * CƠ SỞ DỮ LIỆU PIZZA365
*/

// Khởi tạo mảng gVoucherDb gán dữ liêu từ server trả về 
var gVoucherDb = {
  vouchers: [],

  // voucher methods
  // function get VOUCHER index form VOUCHER id
  // get VOUCHER index from VOUCHER id
  getIndexFromVoucherId: function (paramVoucherId) {
    var vVoucherIndex = -1;
    var vVoucherFound = false;
    var vLoopIndex = 0;
    while (!vVoucherFound && vLoopIndex < this.vouchers.length) {
      if (this.vouchers[vLoopIndex].id === paramVoucherId) {
        vVoucherIndex = vLoopIndex;
        vVoucherFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vVoucherIndex;
  },

  // hàm show menu obj lên modal
  showVoucherDataToForm: function (paramRequsetData) {

    $("#inp-ma-voucher").val(paramRequsetData.maVoucher);
    $("#inp-phan-tram-giam-gia").val(paramRequsetData.phanTramGiamGia);
    $("#inp-ghi-chu").val(paramRequsetData.ghiChu);
    $("#inp-ngay-tao").find("input").val(paramRequsetData.ngayTao);
    $("#inp-ngay-cap-nhat").find("input").val(paramRequsetData.ngayCapNhat);
  }

};

// Khởi tạo mảng gMenuDb gán dữ liêu từ server trả về 
var gMenuDb = {
  menu: [],

  // order methods
  // function get Menu index form Menu id
  // get Menu index from Menu id
  getIndexFromMenuId: function (paramMenuId) {
    var vMenuIndex = -1;
    var vMenuFound = false;
    var vLoopIndex = 0;
    while (!vMenuFound && vLoopIndex < this.menu.length) {
      if (this.menu[vLoopIndex].id === paramMenuId) {
        vMenuIndex = vLoopIndex;
        vMenuFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vMenuIndex;
  },

  // hàm show menu obj lên form
  showMenuDataToForm: function (paramRequsetData) {

    $("#inp-kich-co").val(paramRequsetData.kichCo);
    $("#inp-duong-kinh").val(paramRequsetData.duongKinh);
    $("#inp-suon").val(paramRequsetData.suon);
    $("#inp-salad").val(paramRequsetData.salad);
    $("#inp-so-luong-nuoc-uong").val(paramRequsetData.soLuongNuocUong);
    $("#inp-thanh-tien").val(paramRequsetData.thanhTien);
  }
};

// Khởi tạo mảng gDrinkDb gán dữ liêu từ server trả về 
var gDrinkDb = {
  drink: [],

  // drink methods
  // function get drink index form drink id
  // get drink index from drink id
  getIndexFromDrinkId: function (paramDrinkId) {
    var vDrinkIndex = -1;
    var vDrinkFound = false;
    var vLoopIndex = 0;
    while (!vDrinkFound && vLoopIndex < this.drink.length) {
      if (this.drink[vLoopIndex].id === paramDrinkId) {
        vDrinkIndex = vLoopIndex;
        vDrinkFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vDrinkIndex;
  },

  // hàm show drink obj lên modal
  showDrinkDataToForm: function (paramRequsetData) {

    $("#inp-ma-nuoc-uong").val(paramRequsetData.maNuocUong);
    $("#inp-ten-nuoc-uong").val(paramRequsetData.tenNuocUong);
    $("#inp-don-gia").val(paramRequsetData.donGia);
    $("#inp-ghi-chu").val(paramRequsetData.ghiChu);
    $("#inp-ngay-tao").find("input").val(paramRequsetData.ngayTao);
    $("#inp-ngay-cap-nhat").find("input").val(paramRequsetData.ngayCapNhat);
  },



};

// Khởi tạo mảng gCountryDb gán dữ liêu từ server trả về 
var gCountryDb = {
  countries: [],

  // country methods
  // function get COUNTRY index form COUNTRY id
  // get COUNTRY index from COUNTRY id
  getIndexFromCountryId: function (paramCountryId) {
    var vCountryIndex = -1;
    var vCountryFound = false;
    var vLoopIndex = 0;
    while (!vCountryFound && vLoopIndex < this.countries.length) {
      if (this.countries[vLoopIndex].id === paramCountryId) {
        vCountryIndex = vLoopIndex;
        vCountryFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vCountryIndex;
  },

  // hàm show country obj lên form
  showCountryDataToForm: function (paramRequsetData) {

    $("#inp-country-code").val(paramRequsetData.countryCode);
    $("#inp-country-name").val(paramRequsetData.countryName);
  },

};
/**
 * CƠ SỞ DỮ LIỆU QUẢN LÝ DANH SÁCH REGION
*/

// Khởi tạo mảng gRegionDb gán dữ liêu từ server trả về 
var gRegionDb = {
  regions: [],

  // region methods
  // function get REGION index form REGION id
  // get REGION index from REGION id
  getIndexFromRegionId: function (paramRegionId) {
    var vRegionIndex = -1;
    var vRegionFound = false;
    var vLoopIndex = 0;
    while (!vRegionFound && vLoopIndex < this.regions.length) {
      if (this.regions[vLoopIndex].id === paramRegionId) {
        vRegionIndex = vLoopIndex;
        vRegionFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vRegionIndex;
  },

  // hàm show region obj lên form
  showRegionDataToForm: function (paramRequsetData) {

    $("#inp-update-region-code").val(paramRequsetData.regionCode);
    $("#inp-update-region-name").val(paramRequsetData.regionName);
  },

};

// Khởi tạo mảng gUserDb gán dữ liêu từ server trả về 
var gUserDb = {
  users: [],

  // user methods
  // function get user index form user id
  // get user index from user id
  getIndexFromUserId: function (paramUserId) {
    var vUserIndex = -1;
    var vUserFound = false;
    var vLoopIndex = 0;
    while (!vUserFound && vLoopIndex < this.users.length) {
      if (this.users[vLoopIndex].id === paramUserId) {
        vUserIndex = vLoopIndex;
        vUserFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vUserIndex;
  },

  // hàm show user obj lên form
  showUserDataToForm: function (paramRequsetData) {

    $("#inp-full-name").val(paramRequsetData.fullName);
    $("#inp-email").val(paramRequsetData.email);
    $("#inp-phone").val(paramRequsetData.phone);
    $("#inp-address").val(paramRequsetData.address);
  },

};
/**
 * CƠ SỞ DỮ LIỆU QUẢN LÝ DANH SÁCH ORDER
*/

// Khởi tạo mảng gOrderDb gán dữ liêu từ server trả về 
var gOrderDb = {
  orders: [],

  // order methods
  // function get order index form order id
  // get order index from order id
  getIndexFromOrderId: function (paramOrderId) {
    var vOrderIndex = -1;
    var vOrderFound = false;
    var vLoopIndex = 0;
    while (!vOrderFound && vLoopIndex < this.orders.length) {
      if (this.orders[vLoopIndex].id === paramOrderId) {
        vOrderIndex = vLoopIndex;
        vOrderFound = true;
      }
      else {
        vLoopIndex++;
      }
    }
    return vOrderIndex;
  },

  // hàm show region obj lên form
  showOrderDataToForm: function (paramRequesttData) {

    $("#inp-update-order-code").val(paramRequesttData.orderCode);
    $("#inp-update-pizza-type").val(paramRequesttData.pizzaType);
    $("#inp-update-pizza-size").val(paramRequesttData.pizzaSize);
    $("#inp-update-voucher-code").val(paramRequesttData.voucherCode);
    $("#inp-update-price").val(paramRequesttData.price);
    $("#inp-update-paid").val(paramRequesttData.paid);
  },

};


