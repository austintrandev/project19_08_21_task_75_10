package com.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizza365.model.CUser;

public interface UserRepository extends JpaRepository<CUser, Long> {

}
