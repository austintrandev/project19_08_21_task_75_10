package com.pizza365.voucher.repository;

import com.pizza365.voucher.model.CVoucher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
}
